# BileMo

## Presentation

This is a project made for an Openclassrooms project. The objective was to build an API for phones businesses.

Quality of code has been analysed by [Codacy](https://app.codacy.com/gl/mj-ocr/ocr-bilemo/dashboard?branch=master).

## Requirements

To be able to test the project in a local environment, you will need :

* PHP 7.4+
* composer
* MySQL / MariaDB
* [Symfony-CLI](https://symfony.com/download)

## Installation on a local environment

1. Clone the project : `git clone git@gitlab.com:MarJaM/ocr-bilemo.git`
2. Go into the project repository : `cd ocr-bilemo`
3. Install dependencies : `composer install`
4. Create you .env.local file : `touch .env.local`
5. Add your own [DATABASE_URL](https://symfony.com/doc/current/doctrine.html#configuring-the-database) environment variable into the .env.local file.
6. Setup the dabatase : `php bin/console doctrine:database:create`
7. You have two possibilities here : 
    1. Use the init.dump file provided into the fixtures folder to initiate a set of data `php bin/console doctrine:database:import fixtures/init.dump`.
    2. Initiate the database schema without any data : `php bin/console doctrine:schema:create`
9. Run symfony local server : `symfony server:start`
10. Open your browser, go to http://localhost:8000/docs and enjoy!

If you used the fixtures provided, credentials are :

* bilemo / ApiPassword
* company / ApiPassword
* company2 / ApiPassword