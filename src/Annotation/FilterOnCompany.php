<?php


namespace App\Annotation;

use Doctrine\Common\Annotations\Annotation;

/**
 * @Annotation
 * @Target("CLASS")
 */
class FilterOnCompany
{
    public string $companyColumnName;
}