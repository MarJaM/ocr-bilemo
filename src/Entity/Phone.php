<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\PhoneRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource(
 *     denormalizationContext={"groups"={"phones_write"}},
 *     normalizationContext={"groups"={"phones_read"}},
 *     attributes={"security"="is_granted('ROLE_ADMIN_BILEMO')"},
 *     collectionOperations={
 *         "get"={"security"="is_granted('ROLE_ADMIN_COMPANY')"},
 *         "post"
 *     },
 *     itemOperations={
 *         "get"={"security"="is_granted('ROLE_ADMIN_COMPANY')"},
 *         "put",
 *         "delete"
 *     }
 * )
 * @ORM\Entity(repositoryClass=PhoneRepository::class)
 *
 * @UniqueEntity("model")
 */
class Phone
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"phones_read", "clients_read"})
     */
    private $id;

    /**
     * @Assert\NotBlank()
     * @ORM\Column(type="string", length=127, unique=true)
     * @Groups({"phones_read", "phones_write", "clients_read"})
     */
    private $model;

    /**
     * @Assert\NotBlank()
     * @ORM\Column(type="string", length=5)
     * @Groups({"phones_read", "phones_write", "clients_read"})
     */
    private $screenSize;

    /**
     * @Assert\Type("DateTime")
     * @ORM\Column(type="date")
     * @Groups({"phones_read", "phones_write", "clients_read"})
     */
    private $releaseDate;

    /**
     * @Assert\Type("integer")
     * @ORM\Column(type="integer")
     * @Groups({"phones_read", "phones_write", "clients_read"})
     */
    private $ram;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"phones_read", "clients_read"})
     */
    private $addedToCatalogueDate;

    /**
     * @Assert\NotBlank()
     * @ORM\Column(type="string", length=63)
     * @Groups({"phones_read", "phones_write", "clients_read"})
     */
    private $processor;

    /**
     * @Assert\NotBlank()
     * @ORM\Column(type="string", length=31)
     * @Groups({"phones_read", "phones_write", "clients_read"})
     */
    private $operatingSystem;

    /**
     * Phone constructor.
     */
    public function __construct()
    {
        $this->addedToCatalogueDate = new \DateTime();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getModel(): ?string
    {
        return $this->model;
    }

    public function setModel(string $model): self
    {
        $this->model = $model;

        return $this;
    }

    public function getScreenSize(): ?string
    {
        return $this->screenSize;
    }

    public function setScreenSize(string $screenSize): self
    {
        $this->screenSize = $screenSize;

        return $this;
    }

    public function getReleaseDate(): ?\DateTimeInterface
    {
        return $this->releaseDate;
    }

    public function setReleaseDate(\DateTimeInterface $releaseDate): self
    {
        $this->releaseDate = $releaseDate;

        return $this;
    }

    public function getRam(): ?int
    {
        return $this->ram;
    }

    public function setRam(int $ram): self
    {
        $this->ram = $ram;

        return $this;
    }

    public function getAddedToCatalogueDate(): ?\DateTimeInterface
    {
        return $this->addedToCatalogueDate;
    }

    public function setAddedToCatalogueDate(?\DateTimeInterface $addedToCatalogueDate): self
    {
        $this->addedToCatalogueDate = $addedToCatalogueDate;

        return $this;
    }

    public function getProcessor(): ?string
    {
        return $this->processor;
    }

    public function setProcessor(string $processor): self
    {
        $this->processor = $processor;

        return $this;
    }

    public function getOperatingSystem(): ?string
    {
        return $this->operatingSystem;
    }

    public function setOperatingSystem(string $operatingSystem): self
    {
        $this->operatingSystem = $operatingSystem;

        return $this;
    }
}
