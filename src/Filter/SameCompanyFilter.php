<?php


namespace App\Filter;


use App\Annotation\FilterOnCompany;
use Doctrine\Common\Annotations\Reader;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Query\Filter\SQLFilter;

class SameCompanyFilter extends SQLFilter
{
    private Reader $reader;

    /**
     * @inheritDoc
     */
    public function addFilterConstraint(ClassMetadata $targetEntity, $targetTableAlias): string
    {
        if (null === $this->reader) {
            return '';
        }

        $filterOnCompany = $this->reader->getClassAnnotation($targetEntity->getReflectionClass(), FilterOnCompany::class);

        if (!$filterOnCompany) {
            return '';
        }

        $columnName = $filterOnCompany->companyColumnName;
        try {
            $companyId = $this->getParameter('companyId');
        } catch (\InvalidArgumentException $e) {
            // No user id has been defined
            return '';
        }

        if (empty($columnName) || empty($companyId)) {
            return '';
        }

        return sprintf('%s.%s = %s', $targetTableAlias, $columnName, $companyId);
    }

    public function setAnnotationReader(Reader $reader): void
    {
        $this->reader = $reader;
    }
}