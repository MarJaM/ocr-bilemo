<?php


namespace App\Security;


use ApiPlatform\Core\Bridge\Doctrine\Orm\Paginator;
use App\Entity\Client;
use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class SameCompanyVoter extends Voter
{
    protected function supports(string $attribute, $subject)
    {
        return $attribute === "IS_SAME_COMPANY";
    }

    /**
     * @param string $attribute
     * @param Client $subject
     * @param TokenInterface $token
     * @return bool
     */
    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token)
    {
        if ($subject instanceof Paginator || in_array("ROLE_ADMIN_BILEMO", $token->getRoleNames(), true)) {
            return true;
        }

        /**
         * @var User $user
         */
        $user = $token->getUser();

        return $user->isEqualTo($subject->getCompany());
    }
}